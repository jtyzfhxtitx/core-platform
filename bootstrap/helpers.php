<?php

if (!function_exists('hashParams')) {
    function hashParams(array $params, $key): string
    {
        $plain = '';
        ksort($params);
        foreach ($params as $param)
            $plain .= $param;
        return hash_hmac('sha256', $plain, $key);
    }
}

if (!function_exists('failedResponse')) {

    /**
     * @param string $errorCode
     * @param string $message
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    function failedResponse(string $errorCode, string $message, array $data = [])
    {
        $config = config('response')[$errorCode];
        $response = response()->json([
            'status' => $config['error_code'],
            'message' => $message ? $message : $config['message'],
            'data' => $data,
            'time' => date('Y-m-d H:i:s')
        ], $config['error_code']);
        return $response;
    }
}
