<?php

return [
    0 => [
        'error_code' => 200,
        'message' => 'Success'
    ],

    40300 => [
        'error_code' => 403,
        'message' => 'Not allowed to access'
    ],

    40400 => [
        'error_code' => 404,
        'message' => 'Api not exists'
    ],

    40401 => [
        'error_code' => 404,
        'message' => 'Model not found'
    ],

    40500 => [
        'error_code' => 405,
        'message' => 'Method not allowed'
    ],

    42200 => [
        'error_code' => 422,
        'message' => 'Parameters error'
    ],

    50000 => [
        'error_code' => 500,
        'message' => 'Something went wrong'
    ],

    50001 => [
        'error_code' => 500,
        'message' => 'Application is now in maintenance mode'
    ],

];
