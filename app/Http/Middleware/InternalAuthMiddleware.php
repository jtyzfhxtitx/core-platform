<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Redis;

class InternalAuthMiddleware
{
    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws AuthorizationException
     */
    public function handle($request, Closure $next)
    {
        if (app()->environment() == 'local' && $request->hash == config('app.hash_key')) {

            $this->record('test:'.$request->path());
            return $next($request);
        }
        $this->validateHash($request->hash, $this->toString($request->except('hash')));
        $this->record($request->path());
        return $next($request);
    }

    /**
     * @param $hash
     * @param $plain
     * @throws AuthorizationException
     */
    protected function validateHash($hash, $plain)
    {
        if ($hash != md5($plain . config('app.hash_key')))
            throw new AuthorizationException('Unauthorized');
    }

    /**
     * @param array $components
     * @return string
     */
    protected function toString(array $components)
    {
        $string = '';
        ksort($components);
        foreach ($components as $compo)
            $string .= $compo;
        return $string;
    }

    protected function record($path)
    {
        $dt = Carbon::today()->toDateString();
        $key = $dt.':INCR:'.$path;

        $record = Redis::get($key);
        if(is_null($record)){
            Redis::set($key,1);
        }else{
            Redis::incr($key);
        }
    }
}
