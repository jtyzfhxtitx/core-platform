<?php

namespace App\Http\Controllers;

use App\Http\HttpResponse;
use App\Http\Requests\Channel\ChannelRequest;
use App\Models\JackpotConfig;
use Illuminate\Http\Request;

class ChannelController extends Controller
{
    public function index()
    {
        $data = array([0=>'get works']);

        return HttpResponse::success($data);
    }

    public function store(ChannelRequest $request)
    {
        $data = array([0=>'post works']);

        return HttpResponse::success($data);
    }
}
