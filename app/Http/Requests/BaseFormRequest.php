<?php

namespace App\Http\Requests;

use App\Exceptions\BaseException;
use App\Http\HttpResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class BaseFormRequest extends FormRequest
{
    /**
     * @param Validator $validator
     * @throws BaseException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new BaseException(42200, '', $errors);
    }

    public function authorize()
    {
        return true;
    }
}
