<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof BaseException) {
            return failedResponse($exception->getCode(), $exception->getMessage(), $exception->getData());
        }
        if ($exception instanceof AuthorizationException) {
            return response()->json(config('response')[40300]);
        }
        $this->logException($exception, $request);
        return parent::render($request, $exception);
    }

    private function logException(Exception $exception, Request $request)
    {
        $data = [];
        if (method_exists($exception, 'getData')) {
            $data = $exception->getData();
        }
        Log::error(get_class($exception), [
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'data' => $data,
            'request_api' => $request->path(),
            'request_content' => $request->all(),
        ]);
    }
}
