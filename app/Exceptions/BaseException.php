<?php

namespace App\Exceptions;

use Exception;

class BaseException extends Exception
{
    protected $_data = [];
    public function __construct($code, $message = '', $data = [])
    {
        $this->_data = $data;
        parent::__construct($message, $code);
    }

    public function getData()
    {
        return $this->_data;
    }
}
